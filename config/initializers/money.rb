require 'money/bank/currencylayer_bank'

MoneyRails.configure do |config|
  mclb = Money::Bank::CurrencylayerBank.new
  mclb.access_key = Rails.application.secrets.currency_layer_key
  mclb.update_rates
  mclb.ttl_in_seconds = 60

  config.default_bank = mclb
end
