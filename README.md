# Etsy Products API

This is an API service that displays a JSON list of paginated Etsy products, 
ordered by their Etsy 'scores'.

## Setup

Please contact me for the API keys. They will be provided in a file named 
`secrets.yml`. Drop this file into the `config` directory 
(i.e. as `config/secrets.yml`) in the app before running the server. 

## Run

To setup the API server, run in the shell:

```
$ rails s
```

Go to `localhost:3000` or `localhost:3000/products` to query the API. 
Either route will work (the root `/` route defaults to the `/products` route).

## Input

The input consists of:

* (Optional) A `page` integer parameter from 1 to 5000. Each page will contain 
10 products. Any other input page will result in an error message. 

* (Optional) A `currency` parameter specifying the desired output currency. 
Any other currency will be converted to the specified currency via 
the [CurrencyLayer](https://currencylayer.com/) API. 
The `currency` parameter must be one of: `USD`, `CAD`, `GBP`, or `EUR`. 
Any other input currency will result in an error message. 

## Output

The output is printed as a JSON of listings, in the following format:

* The key `data` contains all the returned Etsy listings

* The key `pagination` contains information on pagination. Within it,
`effective_page` and `next_page` represent the current and next pages 
respectively.

* Each listing has a `title`.

* Each listing contains a list of its display images, under the key 
`images`

* Each listing contains `price` and `currency_code`, where they are 
converted to the specified given currency. If no currency was specified, 
Both attributes are unchanged from the listed originals.


### 'Top' Products

The output products are ordered with respect to their Etsy 'scores'. 
Since I found no 'top sellers' ordering system within the Etsy API, 
I have assumed that ordering by 'score' is a valid way to order by 
top products. The algorithm Etsy uses for calculating listing 'scores' 
is unclear, so I have simply assumed that ordering by such a 'score' 
will display the products that deserve the highest spots first.

Another way of ordering by top products may be fetching 
only from the featured listings, and displaying those listings. 
The implementation would be similar, except instead of using 
`https://openapi.etsy.com/v2/listings/active` as the endpoint 
it would use `https://openapi.etsy.com/v2/featured_treasuries/listings`.

## Example Usage

Here is an example use case, where the URL:
 
```
http://localhost:3000/products?currency=CAD&page=2
``` 

is queried, and the output is returned as a JSON:

```
{
  "data": [
    {
      "listing_id": 540723141,
      "title": "A. Schrader chrome hot water bottle bed warmer patent 1908",
      "price": 51.73,
      "currency_code": "CAD",
      "images": [
        {
          "listing_image_id": 1280348129,
          "url_75x75": "https://img1.etsystatic.com/205/0/5217258/il_75x75.1280348129_5kfm.jpg",
          "url_170x135": "https://img1.etsystatic.com/205/0/5217258/il_170x135.1280348129_5kfm.jpg",
          "url_570xN": "https://img1.etsystatic.com/205/0/5217258/il_570xN.1280348129_5kfm.jpg",
          "url_fullxfull": "https://img1.etsystatic.com/205/0/5217258/il_fullxfull.1280348129_5kfm.jpg"
        },
        ...
        {
          "listing_image_id": 1233123174,
          "url_75x75": "https://img0.etsystatic.com/208/0/5217258/il_75x75.1233123174_iyi1.jpg",
          "url_170x135": "https://img0.etsystatic.com/208/0/5217258/il_170x135.1233123174_iyi1.jpg",
          "url_570xN": "https://img0.etsystatic.com/208/0/5217258/il_570xN.1233123174_iyi1.jpg",
          "url_fullxfull": "https://img0.etsystatic.com/208/0/5217258/il_fullxfull.1233123174_iyi1.jpg"
        }
      ]
    },
    {
      "listing_id": 253994484,
      "title": "Hogan  Ankle Boots/ladies/boots/black/size  UK5/upper leather/suede leather/Rubber Sole",
      "price": 100.39,
      "currency_code": "CAD",
      "images": [
        ...
      ]
    },
    ...
  ],
  "pagination": {
    "effective_limit": 10,
    "effective_offset": 10,
    "next_offset": 20,
    "effective_page": 2,
    "next_page": 3
  }
}

```

## Testing

Tests are included under the `spec` directory. 
To run the tests and check that they pass, run in the shell:

```
$ rspec
```

## Questions

For any enquiries, please [contact me via email](mailto:maryqygao@gmail.com).