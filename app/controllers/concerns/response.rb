module Response
  def json_response(model, status = :ok)
    render json: model, status: status
  end
end
