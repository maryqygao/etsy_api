require 'active_model/validations'

module ErrorHandler
  extend ActiveSupport::Concern
  include Response

  included do
    rescue_from ActiveModel::ValidationError do |e|
      json_response({ message: e.message }, :bad_request)
    end
  end
end