class ProductsController < ApplicationController
  include Response
  include ErrorHandler

  def index
    json_response(ProductsProcessor.new(valid_params).perform)
  end

  private

  def valid_params
    params.permit([
                      :page,
                      :currency
                  ])
  end
end
