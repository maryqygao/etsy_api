class ProductsForm
  include ActiveModel::Model

  attr_accessor :page, :currency
  validates_numericality_of :page,
                            only_integer: true,
                            greater_than_or_equal_to: 1,
                            less_than_or_equal_to: 5000,
                            allow_nil: true
  validates :currency, inclusion: { in: %w(GBP CAD USD EUR), allow_nil: true }

  PRODUCTS_PER_PAGE = 10

  def to_etsy_params
    {
        limit: PRODUCTS_PER_PAGE,
        page: page.present? ? page : 1
    }
  end
end
