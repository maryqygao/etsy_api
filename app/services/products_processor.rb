class ProductsProcessor
  def initialize(products_params)
    @products_params = products_params
  end

  def perform
    products_form = ProductsForm.new(products_params)
    products_form.validate!
    EtsyListingsFetcher.new(products_form).perform
  end

  private

  attr_accessor :products_params
end