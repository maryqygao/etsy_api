require 'net/http'

class EtsyListingsFetcher
  def initialize(products_form)
    @products_form = products_form
  end

  def perform
    {
        data: get_detailed_listings,
        pagination: listings_pagination
    }
  end

  private

  def fetch_etsy_api(path, params = {})
    uri = URI("https://openapi.etsy.com/v2/#{path}")
    params = {api_key: Rails.application.secrets.etsy_api_key}.merge(params)
    uri.query = URI.encode_www_form(params)
    res = Net::HTTP.get_response(uri)
    JSON.parse(res.body)
  end

  def listings
    listings_params = {
        sort_on: 'score',
        fields: %w(listing_id title price currency_code).join(',')
    }
    @listings ||= fetch_etsy_api('listings/active', listings_params.merge(@products_form.to_etsy_params))
  end

  def listing_imgs(listing_id)
    images_params = {
        fields: %w(listing_image_id url_75x75 url_170x135 url_570xN url_fullxfull).join(',')
    }
    @listings_imgs ||= fetch_etsy_api("listings/#{listing_id}/images", images_params)
  end

  def get_detailed_listings
    listings['results'].map do |l|
      if @products_form.currency.present?
        exchanged_price = CurrencyExchanger.new(old_currency: l['currency_code'],
                                                new_currency: @products_form.currency,
                                                price: l['price']).perform
      end
      l.merge(images: listing_imgs(l['listing_id'])['results']).merge(exchanged_price || {})
    end
  end

  def listings_pagination
    listings['pagination']
  end
end