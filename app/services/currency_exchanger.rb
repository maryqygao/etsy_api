require 'money'

class CurrencyExchanger
  def initialize(new_currency:, old_currency:, price:)
    @new_currency = new_currency
    @old_currency = old_currency
    @price = price.to_i
  end

  def perform
    {
        price: new_price,
        currency_code: new_currency
    }
  end

  private

  attr_accessor :new_currency, :old_currency, :price

  def new_price
    return price if new_currency == old_currency
    new_price = Money.new(price * 100, old_currency).exchange_to(new_currency).to_f
    new_price
  end
end