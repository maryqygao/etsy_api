FactoryGirl.define do
  factory :listing, class: Hash do |f|
    title Faker::Lorem.sentence
    listing_id Faker::Number.number(6)
    price Faker::Number.decimal(2, 2)
    currency_code %w(GBP CAD USD EUR).sample

    initialize_with { attributes }
  end
end