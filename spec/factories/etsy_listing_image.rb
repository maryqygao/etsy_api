FactoryGirl.define do
  factory :listing_images, class: Hash do |f|
    listing_image_id Faker::Number.number(6)
    url_75x75 Faker::Internet.url
    url_170x135 Faker::Internet.url
    url_570xN Faker::Internet.url
    url_fullxfull Faker::Internet.url

    initialize_with { attributes }
  end
end