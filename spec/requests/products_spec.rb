require 'rails_helper'
require 'json'

describe 'Products from Etsy', type: :request do
  let!(:stub) do
    stub_request(:get, 'https://openapi.etsy.com/v2/listings/active')
        .with(query: hash_including({'limit' => '10'}))
        .to_return(status: 200,
                   body: {
                       results: etsy_listings,
                       pagination: {
                           effective_page: page || 1
                       }
                   }.to_json)
  end
  let!(:images_stub) do
    stub_request(:get, /^https:\/\/openapi.etsy.com\/v2\/listings\/\w+\/images/)
        .to_return(status: 200,
                   body: {
                       results: etsy_listings_images
                   }.to_json)
  end
  let!(:money_stub) do
    stub_request(:get, 'http://apilayer.net/api/live?access_key=86aa525ef516431647eef282c15e4889&source=USD').
        with(headers: {'Accept' => '*/*', 'Accept-Encoding' => 'gzip;q=1.0,deflate;q=0.6,identity;q=0.3', 'User-Agent' => 'Ruby'}).
        to_return(status: 200, body: {
            'source' => 'USD',
            'quotes' => {
                'USDCAD' => 1.29,
                'USDEUR' => 0.880,
                'USDGBP' => 0.773
            }
        }.to_json)
  end

  let(:etsy_listings) { build_list(:listing, 10) }
  let(:etsy_listings_images) { build_list(:listing_images, 3) }
  let(:data) { JSON.parse(response.body)['data'] }
  let(:product) { data[0] }
  let(:images) { product['images'] }
  let(:listings_pagination) { JSON.parse(response.body)['pagination'] }
  let(:page) { nil }

  before do
    get '/products', params: params
  end

  context 'page and context not specified' do
    let(:params) { {} }

    it 'calls etsy api' do
      expect(stub).to have_been_requested
    end

    it 'returns 10 products' do
      expect(data.size).to eq(10)
    end

    it 'returns the first page' do
      expect(listings_pagination['effective_page']).to eq(1)
    end

    it 'returns title of product' do
      expect(product['title']).to be_a(String)
    end

    it 'returns image urls of product' do
      expect(images[0]).to include('url_fullxfull')
    end
  end

  context 'page is specified' do
    let!(:page) { 2 }
    let(:params) { {page: page} }

    it 'returns 10 products' do
      expect(data.size).to eq(10)
    end

    it 'returns the second page' do
      expect(listings_pagination['effective_page']).to eq(page)
    end
  end

  context 'currency is specified' do
    let(:params) { {currency: 'CAD'} }
    let(:old_price) { etsy_listings[0][:price].to_i }
    let(:old_currency) { etsy_listings[0][:currency_code] }
    let(:new_price) { Money.new(old_price * 100, old_currency).exchange_to('CAD').to_f }

    it 'returns products in CAD currency' do
      expect((data.map { |listing| listing['currency_code'] }).uniq).to eq(['CAD'])
    end

    it 'converts prices to CAD currency' do
      expect(product['price']).to eq(new_price)
    end
  end
end