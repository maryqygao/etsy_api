require 'rails_helper'

describe ProductsForm, type: :model do
  it do
    should validate_numericality_of(:page)
               .only_integer
               .is_greater_than_or_equal_to(1)
               .is_less_than_or_equal_to(5000)
               .allow_nil
  end

  it do
    should validate_inclusion_of(:currency)
               .in_array(%w(GBP CAD USD EUR))
               .allow_nil
  end

  describe '#to_etsy_params' do
    subject { products_form.to_etsy_params }

    context 'page is provided' do
      let(:products_form) { ProductsForm.new(page: 2, currency: 'CAD') }

      it 'outputs params hash to etsy' do
        expect(subject).to eq({limit: 10, page: 2})
      end
    end

    context 'page is not provided' do
      let(:products_form) { ProductsForm.new }

      it 'outputs params hash to etsy and sets page to 1 by default' do
        expect(subject).to eq({limit: 10, page: 1})
      end
    end
  end
end
